import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

public class Main {
    public static void main(String[] args) {

        System.setProperty("webdriver.chrome.driver", "E:\\JavaATQC\\FirstBot\\src\\main\\resources\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.google.com.ua/?hl=ru");
        WebElement element = driver.findElement(By.name("q"));
        element.sendKeys("Electronic Keyboards");
        element.sendKeys(Keys.ENTER);
        List<WebElement> adds = driver.findElements(By.xpath("//h3"));
        for (WebElement e : adds) {
            if (e.getText().contains("Amazon")) {
                e.click();
                break;
            }
        }////span[text()=' Цифровое пианино Casio Privia PX-S1000 Black (PX-S1000BK) ']
        List<WebElement> fnd = driver.findElements(By.xpath("//a"));
        for (WebElement c : fnd) {
            if (c.getText().contains("Casio CT-X700 61-Key Portable Keyboard")) {
                c.click();
                break;
            }
        }
        WebElement btn = driver.findElement(By.name("submit.add-to-cart"));
        btn.click();
    }
}
