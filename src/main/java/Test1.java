import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.annotations.AfterClass;
import java.util.concurrent.TimeUnit;
public class Test1 {
    public WebDriver driver;
    public WebElement product;
    public WebElement buy;
    public boolean result;

    @Test(priority = 1)
    void pageLoaded() {
        driver.get("https://demo.opencart.com/index.php?route=common/home");
        WebElement logo = driver.findElement(By.xpath("//a[text()='Your Store']"));
        boolean active = logo.isDisplayed();
        Assert.assertTrue(active, "Logo is not displayed");
    }

    @Test(priority = 2)
    void clickBuyButton() {
        product = driver.findElement(By.xpath("//img[@alt='iPhone']"));
        product.click();
        buy = driver.findElement(By.xpath("//button[text()='Add to Cart']"));
        buy.click();
    }

    @Test(priority = 3)
    void oneItemAdded() throws InterruptedException {
        boolean actual = false;
        for (int i = 0; i < 10; i++) {
            product = driver.findElement(By.xpath("//span[@id='cart-total']"));
            actual = product.getText().contains("1 item(s)");
            if (actual) {
                break;
            } else {
                Thread.sleep(50);
            }
        }
        Assert.assertTrue(actual, "Cart does not contain 1 item");
    }

    @Test(priority = 4)
    void ClickToBrand(){
        product = driver.findElement(By.xpath("//a[text()='Apple']"));
        result = product.isDisplayed();
        Assert.assertTrue(result,"Brand is a not displayed");
        product.click();
    }

    @Test(priority = 5)
    void AddToCart() throws InterruptedException {
        product = driver.findElement(By.xpath("//div[@class='caption' and .//h4[.='iPhone']]//following-sibling::div//span[.='Add to Cart']"));
        result = product.isDisplayed();
        Assert.assertTrue(result, "Button is a not displayed");
        product.click();
        Thread.sleep(1000);
    }

    @Test(priority = 6)
    void MessageIsOnWindow(){
        product = driver.findElement(By.xpath("//div/a[.='iPhone']"));
        result = product.isDisplayed();
        Assert.assertTrue(result,"Message is a not displayed");
    }

    @Test(priority = 7)
    void ClickCart(){
        product = driver.findElement(By.xpath("//div[@id='cart']/button"));
        result = product.isDisplayed();
        Assert.assertTrue(result,"Cart is a not displayed");
        product.click();
    }

    @Test(priority = 8)
    void ClickViewCart() throws InterruptedException {
        product=driver.findElement(By.xpath("//a/strong[.=' View Cart']"));
        product.click();
    }

    @Test(priority = 9)
    void ChangeCart() {
        boolean actual = false;
        product = driver.findElement(By.xpath("//a [text() = 'iPhone']"));
        actual = product.getText().contains("iPhone");
        if (actual) {
            product = driver.findElement(By.xpath("//input [contains (@value, '2')]"));
            product.sendKeys(Keys.BACK_SPACE);
            product.sendKeys(Keys.NUMPAD2);
            product = driver.findElement(By.xpath("//button [contains (@type, 'submit')]"));
            product.click();
        }
    }

    @BeforeClass
    void before(){
        WebDriverManager.chromedriver().setup();
        driver= new ChromeDriver();
        driver.manage().window().maximize();
    }

    @AfterClass
    void postCondition(){
        driver.quit();
    }
}
//    @Test(priority = 1)
//    void pageLoaded() {
//        driver.get("https://www.amazon.com/Casio-61-Key-Portable-Keyboard-CTX700/dp/B0794RNK5V/ref=sr_1_20?_encoding=UTF8&c=ts&dchild=1&keywords=Electronic+Keyboards&qid=1617054642&s=musical-instruments&sr=1-20&ts_id=11970001");
//        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//        WebElement logo = driver.findElement(By.xpath("//a[@href='/ref=nav_logo']"));
//        boolean active =  logo.isDisplayed();
//        Assert.assertTrue(active, "Logo is not displayed");
//    }
//
//    @Test(priority = 2)
//    void cartIsEmpty() {
//        cart = driver.findElement(By.xpath("//span[@class='nav-cart-icon nav-sprite']"));
//        cart.click();
//        WebElement text = driver.findElement(By.xpath("//h2"));
//        boolean active = text.getText().equals("Your Amazon Cart is empty");
//        Assert.assertTrue(active, "Your Amazon Cart is not empty");
//    }
//
//    @Test(priority = 3)
//    void cartOneItem() throws InterruptedException {
//        driver.navigate().back();
//        WebElement btn = driver.findElement(By.name("submit.add-to-cart"));
//        btn.click();
//        boolean active = false;
//        for (int i = 0; i<20; i++) {
//            cart = driver.findElement(By.xpath("//span[text()='1']"));
//            active = cart.getText().contains("1");
//            if (active) {
//                break;
//            } else {
//                Thread.sleep(50);
//            }
//        }
//        Assert.assertTrue(active, "Your Amazon Cart does not contain 1 item");
//    }
//
//    @BeforeClass
//    public void before() {
//        WebDriverManager.chromedriver().setup();
//        driver = new ChromeDriver();
//        driver.manage().window().maximize();
//    }
//
//    @AfterClass
//    void postConditions() {
//        driver.quit();
//    }
//}
